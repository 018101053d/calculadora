<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora</title>
    <link rel='stylesheet' type='text/css'  href="css/style.css">
    
</head>
<body>
    <form action="#" method="post">
      <legend class="titulo" >Calculadora</legend>
      <div class="input">
          <input  type="number"step="any" name="txtNro1" required>
          <input type="number" step="any" name="txtNro2" >
     </div>
      <br>
           <div class="botones">
               <input  type="submit" name="btnSumar" value="+">
               <input  type="submit" name="btnRestar" value="-">
               <input  type="submit" name="btnMultiplicar" value="*">
               <input  type="submit" name="btnDividir" value="/">
               <input  type="submit" name="btnFactorial" value="Fact">
               <input  type="submit" name="btnPotencia" value="^">
               <input  type="submit" name="btnSeno" value="Sin">
               <input  type="submit" name="btnTangente" value="Tan">
               <input  type="submit" name="btnPorcentaje" value="%">
               <input  type="submit" name="btnRaiz2" value="√">
               <input  type="submit" name="btnRaizN" value="n √">
               <input  type="submit" name="btnInversa" value="Inv">    
          </div>
        
    
   
    <?php
     include('calculadora.php');
     $n1=$_POST['txtNro1'];
     $n2=$_POST['txtNro2'];
     $cal=new Calculadora();
     $cal->n1=$n1;
     $cal->n2=$n2;
     if(isset($_POST['btnSumar'])){   
         $sumar=$cal->sumar();
         ?><p><?php echo $sumar;?><p><?php
     }elseif(isset($_POST['btnRestar'])){
         $restar=$cal->restar();
         ?><p><?php echo $restar;?><p><?php
     }elseif(isset($_POST['btnMultiplicar'])){
         $multi=$cal->multiplicar();
         ?><p><?php echo $multi;?><p><?php
     }elseif(isset($_POST['btnDividir'])){
         $divi=$cal->dividir();
         ?><p><?php echo $divi;?><p><?php
     }elseif(isset($_POST['btnFactorial'])){
          $fac=$cal->Fact($n1);
         ?><p><?php echo $fac;?><p><?php
     }elseif(isset($_POST['btnPotencia'])){
          $pow=$cal->pow($n1,$n2);
          ?><p><?php echo $pow;?><p><?php
     }elseif(isset($_POST['btnSeno'])){
          $seno=$cal->Seno($n1);
          ?><p><?php echo $seno;?><p><?php
     }elseif(isset($_POST['btnTangente'])){
          $tan=$cal->Tangente($n1);
          ?><p><?php echo $tan;?><p><?php
     }elseif(isset($_POST['btnPorcentaje'])){
          $porcen=$cal->Porcentaje();
          ?><p><?php echo $porcen;?><p><?php
     }elseif(isset($_POST['btnRaiz2'])){
          
          ?><p><?php echo sqrt($n1);?><p><?php
     }elseif(isset($_POST['btnRaizN'])){
          $raizn=$cal->RaizN();
          ?><p><?php echo $raizn;?><p><?php
     }elseif(isset($_POST['btnInversa'])){
          $inv=$cal->Inversa();
          ?><p><?php echo $inv;?><p><?php
     }

    ?>
    </form>
  
		<footer >
               <br>
                    Todos los derechos reservados
               <br>
                    Frank Montalico Castro
               <br>
		</footer>
	
</body>
</html>
