<?php 
    class Calculadora{
        //atributos
        public $n1;
        public $n2;
        //metodos
     
        public function sumar(){
            return  $this->n1+$this->n2;
          
        }
        public function restar(){
            return $this->n1- $this->n2;
        }
        public function multiplicar(){
            return $this->n1* $this->n2;
        }
        public function dividir(){
           if ($this->n2!=0){
                return $this->n1 /$this->n2;
           }else{
               return "Error de division";
           }
        }
        public function Fact($nro)
        {
            if($nro == 0)
            {
                return 1;
            }
            else
            {
                return $nro * $this-> Fact($nro - 1);
            }
        }
        public function pow($a,$b){
             if($b==0){
                 return 1;
             }else{
                 return $a * pow($a,$b-1);
             }
        }
        public function Seno($nro) {
            return (sin(deg2rad($nro)));
        }
        public function Tangente($nro) {
            return (tan(($nro * pi()) / 180));
        }

        public function Coseno($nro) {
            return (cos(($nro * pi()) / 180));
        }
        public function Porcentaje(){
            return $this->n1/100;
        }
        public function RaizN(){
            return pow($this->n1,1/$this->n2);
        }
        public function Inversa(){
            return 1/$this->n1;
        }
    }
?>